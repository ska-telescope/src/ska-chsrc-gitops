# Project Moved

This project has been migrated to this new location: [deployments/chsrc/ska-src-chsrc-services-cd](https://gitlab.com/ska-telescope/src/deployments/chsrc/ska-src-chsrc-services-cd).

